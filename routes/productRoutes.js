const express = require("express");
const router = express.Router();
const auth = require("../auth");

const productControllers = require("../controllers/productControllers")

// Create product
router.post("/create", auth.verify, productControllers.createProduct);

// Retrieve all active product
router.get("/allActiveProduct", auth.verify, productControllers.retrieveAllActive);

// Retrieve all product
router.get("/allProduct", productControllers.getAllProducts);



// Retrieve single product
router.get("/:id", productControllers.getProduct);

// Retrieve single product
router.get("/buy/:_id", productControllers.buyProduct);


// Update specific product
router.put("/update/:_id", auth.verify, productControllers.updateProduct);

// Archive product
router.patch("/:_id/archive", auth.verify, productControllers.archiveProduct);

// UnArchive product
router.patch("/:_id/unArchived", auth.verify, productControllers.UnArchiveProduct);



module.exports = router;