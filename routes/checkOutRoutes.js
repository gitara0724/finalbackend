const express = require("express")
const router = express.Router()
const auth = require("../auth")

const checkOutControllers = require("../controllers/checkOutControllers")

router.post("/", auth.verify, checkOutControllers.checkOut)

module.exports = router
