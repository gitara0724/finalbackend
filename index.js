// Dependencies

const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");


const userRoutes = require("./routes/userRoutes");
const productRoutes = require("./routes/productRoutes");
const cartRoutes = require('./routes/cartRoutes');
const checkOutRoutes = require("./routes/checkOutRoutes");

const app = express();


// Middlewares
app.use(cors());
app.use(express());

app.use(express.json());
app.use(express.urlencoded({extended:true}));


// 
app.use("/users", userRoutes);
app.use("/products", productRoutes);
app.use("/cart", cartRoutes);
app.use("/checkOut", checkOutRoutes);

// Database connection
mongoose.set('strictQuery', true);

mongoose.connect("mongodb+srv://admin:admin@zuittbatch243.cyvdebm.mongodb.net/capstone2?retryWrites=true&w=majority", {
	useNewUrlParser:true,
	useUnifiedTopology:true
})

mongoose.connection.on("error", console.error.bind(console, "connection error"));
mongoose.connection.once("open", () => console.log("Now connected to MongoDB Atlas"));


app.listen(process.env.PORT || 4000, () => {
	console.log(`API is now online on port ${process.env.PORT||4000}`);
})