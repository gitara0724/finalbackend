const Product = require("../models/Product");
const auth = require("../auth");


/*// Create products
module.exports.createProduct = (req, res) => {

	let newProduct = new Product({
		name: req.body.name,
		description: req.body.description,
		price: req.body.price,
		stock: req.body.stock
	})

	const token = req.headers.authorization;
	const userData = auth.decode(token);
	console.log(userData);

	if(userData.isAdmin){

		const product = Products.findOne({
			name: req.body.name
		})

		let productExists;

		if(product){
			// return res.send({productExists: true})
			return false;
		}

		let productCreated;

		newProduct.save().then(result => {
			console.log(result)

			// res.send({productCreated: true})
			return true
		}).catch(error => {
			console.log(error);
			res.send(false);
		})
	} else {
		let isUserAdmin;
		// res.send({isUserAdmin: false})
		return false;
	}
}*/


// Create a product 
module.exports.createProduct = async (request, response) => {
    const userData = auth.decode(request.headers.authorization);

    try {
        const {name, price, description, image,  stock} = request.body;
        let isUserAdmin;

        if(userData.isAdmin) {
            // must contain image
            let hasImage;
            if (image === "") {
                return response.status(400).send({hasImage: false});
            }

            const newProduct = new Product(
                {
                    name,
                    price,
                    description,
                    image,
                    stock
                }
            )

            // // check if product exists in shop
            // const product = await Product.findOne({ title: request.body.title });
            // let productExists;
            // if (product)
            // return response.status(400).send({ productExists: true });

            let productCreated;
            await newProduct.save();
            response.send({ productCreated: true }); 
        } else {
            return response.status(400).send({ isUserAdmin: false });
        }
    } catch (err) {
        console.log(err);
        return response.status(500).send(false);
    }
}


// Retrieve all active products
module.exports.retrieveAllActive = (req, res) => {
	const token = req.headers.authorization;
	const userData = auth.decode(token);

	if(!userData.isAdmin || userData.isAdmin){
	return Product.find({isActive: true}).then(result => {
		res.send(result);
	}).catch(err => {
		res.send(err);
	})
} 
}

// Retrieve all products
module.exports.getAllProducts = (req, res) => {

		return Product.find({}).then(result => res.send(result)).catch(err => {
			console.log(err);
			res.send(err);
		})
	}



// Retrieve single product
module.exports.buyProduct = (req, res) => {
	const productId = req.params._id;
console.log("ID@1", productId)
	return Product.findById(productId).then(result => {
		console.log('RESULT', result)
		res.send(result);
	}).catch(error => {
		res.send(error);
	})
}


module.exports.getProduct = (req, res) => {
	const productId = req.params.id;
	console.log("ID", productId)
	return Product.findById(productId).then(result => {
		console.log('RESULT@2', result)
		res.send(result);
	}).catch(error => {
		res.send(error);
	})
}


// Update product
module.exports.updateProduct = (req, res) => {
	const token = req.headers.authorization;
	const userData = auth.decode(token);

	let updatedProduct = {
		name: req.body.name,
		description: req.body.description,
		price: req.body.price,
		stock: req.body.stock,
		isActive: req.body.isActive
	}

	const productId = req.params._id;

	if(userData.isAdmin){
		return Product.findByIdAndUpdate(productId, updatedProduct, {new:true}).then(result => {
			res.send("Product updated successfully!")
		}).catch(err => {
			res.send(err);
		})
	} else{
		return res.send(`You don't have access to this page!`)
	}
}


// Archive
module.exports.archiveProduct = (req, res) => {
	const token = req.headers.authorization;
	const userData = auth.decode(token);

	let archivedProduct = {
		isActive: false
	}

	const productId = req.params._id;

	if(userData.isAdmin){
		return Product.findByIdAndUpdate(productId, archivedProduct, {new:true}).then(result => {

			return res.send(true);
		}).catch(err => {
			console.log(err);
			res.send(false);
		})
	} else{
		return res.send(false)
	}
}

// unarchive Product
module.exports.UnArchiveProduct = (req, res) => {
	const token = req.headers.authorization;
	const userData = auth.decode(token);

	let unarchivedProduct = {
		isActive: true
	}

	const productId = req.params._id;

	if(userData.isAdmin){
		return Product.findByIdAndUpdate(productId, unarchivedProduct, {new:true}).then(result => {

			return res.send(true);
		}).catch(err => {
			console.log(err);
			res.send(false);
		})
	} else{
		return res.send(false)
	}
}