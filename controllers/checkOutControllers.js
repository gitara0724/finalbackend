const mongoose = require("mongoose");
const User = require("../models/User");
const Product = require("../models/Product");
const Cart = require("../models/Cart");

const auth = require("../auth")

module.exports.checkOut = async (req, res) => {
    const token = req.headers.authorization
    const userInst = auth.decode(token)


    if(!userInst.isAdmin){
    const userModel = await User.findById(userInst.id)

    const cartContents = await Cart.where({
        user: userInst.id,
    }).all()

    if (!cartContents.length)
        res.send({
            status: false,
            message: "Cart is empty.",
        })

    let orderTotal = 0

    const orderId = new mongoose.Types.ObjectId()

    const orderItems = cartContents.map(async (item) => {
        const productInst = await Product.findById(item.product)

        productInst.orders.push(orderId)

        await Cart.findByIdAndDelete(item.id)

        await Product.findByIdAndUpdate(productInst.id, {
            stock: productInst.stock - item.quantity,
            orders: productInst.orders,
        })

        orderTotal += productInst.price * item.quantity

        return {
            productId: productInst.id,
            productName: productInst.name,
            productPrice: productInst.price,
            quantity: item.quantity,
            subtotal: productInst.price * item.quantity,
        }
    })

    userModel.orders.push({
        _id: orderId,
        products: await Promise.all(orderItems),
        totalAmount: orderTotal
    })

    await userModel.updateOne({
        orders: userModel.orders,
    })

     res.send({
        status: true,
        message: "Order added successfully",
        data: userModel.orders,
    })
    }
    else{
        return res.send("You are admin, you cannot proceed with order.")
    }
}